'use-strict';

const aws = require('aws-sdk');
aws.config.region = 'us-east-1';
const lambda = new aws.Lambda();

const cacheService = require('../cache-manager/cacheService');

module.exports.lambdaInvoker = function (event, context, callback, logger) {
    var params = {
        FunctionName: 'simpplr-authorizer-load-permissions',
        InvocationType: 'RequestResponse',
        LogType: 'Tail',
        Payload: '{ "tenantId" : "1001", "userId" : "vinod", "keyName" : "permissions" }'
    };

    lambda.invoke(params, function (err, data) {
        if (err) {
            logger.error('load Permissions Lambda::',err);
            context.fail(err);
        } else {
            logger.info('Load Permissions Lambda:: ' + data.Payload);
            cacheService.updateCache('1001#vinod#permissions', data, logger);
            context.succeed('Load Permissions Lambda:: ' + data.Payload);
        }
    })
};