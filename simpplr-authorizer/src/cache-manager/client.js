'use-strict';

const redis = require('redis');

const redisOptions = {
    host: process.env.REDIS_URL || '127.0.0.1',
    port: process.env.REDIS_PORT || 6379,
    password: process.env.REDIS_PASS || null
}

const client = redis.createClient(redisOptions);

module.exports = client;

