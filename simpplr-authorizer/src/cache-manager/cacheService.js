'use-strict';

const cachingClient = require('./client');

cachingClient.on('connect', function() {
    console.log('Redis:: Client connected...');
});

cachingClient.on('error', function (err) {
    console.log('Redus:: Something went wrong...' + err);
});

const testFunction = () => {
    cachingClient.set('my test key1', 'my test value');
    cachingClient.get('my test key1', function (error, result) {
        if (error) {
            console.log(error);
            throw error;
        }
        console.log('Redis::testFunction::' + result);
    });
}

module.exports.updateCache = (hashKey, obj, logger) => {
    logger.debug('Redis:: Key:', hashKey, ', Obj:', obj);
    cachingClient.set(hashKey, JSON.stringify(obj));
    return cachingClient.get(hashKey, function (error, result) {
        if (error) {
            logger.error(error);
            throw error;
        }
        logger.debug('Redis::updateCache::' + result);
    });
}

module.exports.checkCache = (hashKey, logger) => {
    logger.debug('Redis:: key:', hashKey);
    return cachingClient.get(hashKey, function (error, result) {
        if (error) {
            logger.error(error);
            throw error;
        }
        logger.debug('Redis:: checkCache:' + result);
        return result;
    });
}