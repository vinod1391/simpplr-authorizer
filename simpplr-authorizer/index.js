'use-strict';

const { Logger } = require('@vinod827/simpplr-aws-logging-framework/src/simpplr-pino');

const logger = new Logger('authorization', null);

const cacheService = require('./src/cache-manager/cacheService');

const invoker = require('./src/service/invoker');

let response;

/**
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 * @param {Object} context
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 */
exports.handler = function (event, context, callback) {
    logger.debug('testing lambda');
    console.log('Im debugging my code');
    try {
        const permissions = cacheService.checkCache('1001#vinod#permissions', logger);
        if (permissions !== null || permissions !== undefined) {
            return permissions;
        } else {
            const res = invoker.lambdaInvoker(event, context, callback, logger);
            logger.debug('Res::', res);
            response = {
                'statusCode': 200,
                'body': JSON.stringify({
                    message: res,
                })
            }
        }
    } catch (err) {
        logger.error(err);
        return err;
    }
    return response
}
